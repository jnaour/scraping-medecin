Scraping des données médecins ameli
----------------------------------------------

Obectif:
- Scrapping des données d'ameli pour améliorer la base d'enquête
- Limitation de l'extraction sur le département 35 pour voir l'intérêt de ces données

Code développé en python (3.6.3) en utilisant les librairies Selenium, BeautifulSoup, pandas. Code sous forme d'un notebook jupyter.

Table de résultats:

Le fichier medic_list.csv est au format csv avec pour séparateur ';'


Variables:
- nom: Nom du médecin
- prenom: Prénom du médecin
- tel: Numéro de téléphone au format 06 06 06 06 06
- adresse: Adresse complète du médecin
- cp: Code postal
- ville: Ville
- carte_vitale: si oui (1) ou non (0) le médecin prend la carte vitale
- honoraires: Honoraire du médecin
- convention: Convention
- centre_de_soin: Si oui (1) ou non (0) c'est un centre de soin. Quelques éléments de l'annuaire ameli sont en fait des centres de soin.
